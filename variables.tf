#variable "aws_access_key" {
#  type        = string
#  description = "AWS Access Key"
#  sensitive   = true
#}

#variable "aws_secret_key" {
#  type        = string
#  description = "AWS Secret Key"
#  sensitive   = true
#}

variable "aws_region" {
  type        = string
  description = "Tokyo Region"
  default     = "ap-northeast-1"
}


variable "elasticapp" {
  default = "tfreduxstore-new"
}

variable "elasticappdesc" {
  default = "tfreduxstore-new-desc"
}

variable "beanstalkappenv" {
  default = "tfreduxstore-new-env"
}
variable "solution_stack_name" {
  type = string
}
variable "tier" {
  type = string
}
 
variable "vpc_id" {}
variable "public_subnets" {}
variable "elb_public_subnets" {}
