vpc_id              = "vpc-05758e1d2282b5ede"
Instance_type       = "t2.medium"
minsize             = 1
maxsize             = 2
public_subnets     = ["subnet-00841270e697aad35", "subnet-0e94cf58644070a7d"] # Service Subnet
elb_public_subnets = ["subnet-00841270e697aad35", "subnet-0e94cf58644070a7d"] # ELB Subnet
tier = "WebServer"
solution_stack_name= "64bit Amazon Linux 2 v5.6.4 running Node.js 16"
